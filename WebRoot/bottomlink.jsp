<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<%@page import="util.Info"%>
<%@page import="dao.CommDAO"%>
<%@page import="util.PageManager"%>
<!-- Footer -->
		<footer id="footer">
			<div class="container">
				<div class="footer-widgets">
					<div class="row">
						<div class="col-md-3">
							<div class="footer-about-us">
								<h5 class="heading-white">关于我们</h5>
                               </div>

						</div> <!-- end about us -->

						<div class="col-md-3">
							
							<div class="office-1">
								<h5 class="heading-white">联系我们</h5>
								<p>电话: 000-00000000</p>
								<p>邮箱: 000000@qq.com</p>
                                <p>地址: 中国</p>
							</div>
						</div> <!-- end stay in touch -->

						<div class="col-md-3">
							<h5 class="heading-white">影视资讯</h5>
							<div class="footer-posts">
                            <%
		  for(HashMap beq:new CommDAO().select("select * from newsproclaim where 1=1 order by id desc ",1,3)){

		  %>     
                            <div class="footer-entry">
									<h6>
										<a href="newsexplain.jsp?id=<%=beq.get("id") %>" target="_blank" ><%=Info.ensubStr(beq.get("biaoti"),20)%></a>
									</h6>
									<span><%=beq.get("addtime").toString().substring(0,10) %></span>
								</div> <!-- end entry -->
<%
		
	 }
%>                    

							
							</div> <!-- end footer posts -->
						</div> <!-- end latest posts -->

						<div class="col-md-3">
							<h5 class="heading-white">电影信息</h5>
							<ul class="recent-works">
                            <%
									   
		  for(HashMap tyn:new CommDAO().select("select * from dianyingxinxi where xuanchuanhaibao<>'' order by id desc ",1,6)){
			
		  %>
                            <li>
									<a href="dianyingxinxidetail.jsp?id=<%=tyn.get("id")%>" >
										<img src="<%=tyn.get("xuanchuanhaibao")%>"   width="85" height="85">
									</a>
								</li>
                                <%
								
				  	}
					
				  %>


								
							</ul>
						</div> <!-- end recent works -->
					</div> <!-- end row -->
				</div> <!-- end footer widgets -->			
			</div> <!-- end container -->

			<div class="bottom-footer">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-center">
							<div class="copyright">
								Copyright &copy; 电影院网上售票系统 版权所有
							</div>
						</div> <!-- end col -->
					</div> <!-- end row -->
				</div> <!-- end container -->
			</div> <!-- end bottom footer -->
		</footer> 
        

		<div id="back-to-top">
			<a href="#top"><i class="fa fa-angle-up"></i></a>
		</div>

        <!-- end footer -->

	</div> <!-- end main-wrapper -->
	

	<!-- jQuery Scripts -->
	<script src="22vd/js/jquery.min.js" ></script>
 	<script type="text/javascript" src="22vd/js/lightbox.min.js" ></script>	
	<script type="text/javascript" src="22vd/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="22vd/js/bootstrap-hover-dropdown.min.js" ></script>
	<script type="text/javascript" src="22vd/js/owl.carousel.min.js" ></script>
	<script type="text/javascript" src="22vd/js/jquery.countTo.js" ></script>
	<script type="text/javascript" src="22vd/js/jquery.appear.js" ></script>
	<script type="text/javascript" src="22vd/js/plugins-scroll.js" ></script>
	<script type="text/javascript" src="22vd/js/isotope.pkgd.min.js" ></script>
	<script type="text/javascript" src="22vd/js/jquery.easing.min.js" ></script>
	<script type="text/javascript" src="22vd/js/jquery.easypiechart.min.js" ></script>
	<script type="text/javascript" src="22vd/js/jquery.flexslider-min.js" ></script>
	<script type="text/javascript" src="22vd/js/imagesloaded.pkgd.min.js" ></script>
	<script src="22vd/js/wow.min.js" ></script>

	<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="22vd/rs-plugin/js/jquery.themepunch.tools.min.js" ></script>
	<script type="text/javascript" src="22vd/rs-plugin/js/jquery.themepunch.revolution.min.js" ></script>

	<!-- Custom Theme JavaScript -->
    <script src="22vd/js/scripts.js" ></script>

	<!-- Revolution Slider -->
	<script type="text/javascript">
				jQuery(document).ready(function() {
					   jQuery('.tp-banner').revolution(
						{
							delay:9000,
							startwidth:1170,
							startheight:680,
							hideThumbs:10,

							navigationType:"bullet",							
							navigationStyle:"preview1",

							hideArrowsOnMobile:"on",
							
							touchenabled:"on",
							onHoverStop:"on",
							spinner:"spinner4"
						});
				});
	</script>


