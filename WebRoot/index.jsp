<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<%@page import="util.Info"%>
<%@page import="dao.CommDAO"%>
<%@page import="util.PageManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="content-type" content="text/html;charset=gbk">
	<title>电影院网上售票系统</title>
	<link rel="stylesheet" type="text/css" href="22vd/css/bootstrap.min.css" media="screen">
	<link rel="stylesheet" type="text/css" href="22vd/rs-plugin/css/settings.css"  media="screen" />
	<link rel="stylesheet" href="22vd/css/owl.carousel.css" >
	<link rel="stylesheet" type="text/css" href="22vd/css/lightbox.min.css"  media="all"/>   
	<link rel="stylesheet" type="text/css" href="22vd/css/stroke-gap-icons.css"  media="screen" />
	<link rel="stylesheet" type="text/css" href="22vd/css/pe-icon-7-stroke.css"  media="screen" />
	<link rel="stylesheet" type="text/css" href="22vd/css/elegant-icons.css"  media="screen" />
	<link rel="stylesheet" type="text/css" href="22vd/css/font-awesome.css"  media="screen" />
	<link rel="stylesheet" type="text/css" href="22vd/css/style.css"  media="screen" />
	<link rel="stylesheet" type="text/css" href="22vd/css/responsive.css"  media="screen" />
	<link rel="stylesheet" type="text/css" href="22vd/rs-plugin/css/settings.css"  media="screen" />
	<link rel="stylesheet" type="text/css" href="22vd/css/animate.css"  media="screen" />
	<link rel="stylesheet" type="text/css" href="22vd/css/flexslider.css"  media="screen" />
    <link rel="stylesheet" type="text/css" href="22vd/css/css_whir.css"  media="screen" />

<script type="text/javascript">
 <%
String error = (String)request.getAttribute("error"); 
if(error!=null)
{
 %>
 alert("用户名或密码错误");
 <%}%>
 
  <%
String random = (String)request.getAttribute("random"); 
if(random!=null)
{
 %>
 alert("验证码错误");
 <%}%>
 
 popheight = 39;
 </script>
</head>

<body>
	<%@ include file="headerlink.jsp"%>

	

	<!-- Featured Works -->
		<section class="featured-works">
			<div class="container-fluid clearfix">
				<div class="row">
					<h2 class="bg-dark mb0 heading-white text-center">电影信息</h2>
					<div id="owl-featured-works" class="owl-carousel owl-theme">
                    <style>
					.owl-buttons{ display:none}
					</style>
                    <%
									   
		  for(HashMap spz:new CommDAO().select("select * from dianyingxinxi where xuanchuanhaibao<>'' order by id desc ",1,5)){
			
		  %>
                     <div class="item">
							<a href="dianyingxinxidetail.jsp?id=<%=spz.get("id")%>" >
								<img src="<%=spz.get("xuanchuanhaibao")%>"  alt="<%=spz.get("dianyingmingcheng")%>" height="260">
							</a>
							<div class="portfolio-overlay">
                            	<div class="featured-item-description">
                            		<h3><a href="dianyingxinxidetail.jsp?id=<%=spz.get("id")%>" ><%=spz.get("dianyingmingcheng")%></a></h3>
                    			</div>
							</div>
						</div> <!-- end first item -->
			<%
				  	}
					
				  %>

						
					</div>	
				</div> <!-- end row -->
			</div> <!-- end container -->
		</section> <!-- end featured works -->

	

	

	<!-- From Blog -->
		<section class="section-wrap from-blog">
			<div class="container">			
				<h2 class="text-center">影视资讯</h2>
				<p class="subheading text-center">News</p>
				<div class="main-border blue"></div>

				<div class="blog-articles mt50">
					<div class="col-xs-12 text-center">
                    <%
		  for(HashMap flq:new CommDAO().select("select * from newsproclaim where 1=1 order by id desc ",1,3)){

		  %>     
                    <div class="entry-text mt40">
							<h3>
								<a href="newsexplain.jsp?id=<%=flq.get("id") %>" target="_blank" ><%=Info.ensubStr(flq.get("biaoti"),20)%></a>
							</h3>
							<span class="blog-data"><%=flq.get("addtime").toString().substring(0,10) %></span>	
						</div><!-- end entry -->
					 <%
		
	 }
%>                        

						
					</div> <!-- end column-->
				</div> <!-- end blog-article -->
			</div> <!-- end container -->
		</section> <!-- end from blog -->

	<%@ include file="bottomlink.jsp"%>
	
	
	  
</body>
</html>

