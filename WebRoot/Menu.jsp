<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<%@page import="util.Info"%>
<%@page import="dao.CommDAO"%>
<%@page import="util.PageManager"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>

<title>电影院网上售票系统</title>

<link rel="icon" href="images/icon/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script src="javascript/jquery.js"></script>
<script src="javascript/plug-ins/customScrollbar.min.js"></script>
<script src="javascript/plug-ins/echarts.min.js"></script>
<script src="javascript/plug-ins/layerUi/layer.js"></script>
<script src="editor/ueditor.config.js"></script>
<script src="editor/ueditor.all.js"></script>
<script src="javascript/plug-ins/pagination.js"></script>
<script src="javascript/public.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
</head>
<body>
<div class="main-wrap">
	<div class="side-nav">
		<div class="side-logo">
			<div class="logo" style="float:left">
			<span class="logo-ico">
					<i class="i-l-1"></i>
					<i class="i-l-2"></i>
					<i class="i-l-3"></i>
				</span>
				<strong>电影院网上售票系统</strong>
			</div>
		</div>
		
		<nav class="side-menu content mCustomScrollbar" data-mcs-theme="minimal-dark">
			
			<ul>
				<li>
					<dl>
						<dt>
							<i class="icon-columns"></i>系统用户管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="xtyhgl.jsp" target=main>管理员管理</a>
						</dd>
						<dd>
							<a href="gukezhuce_list.jsp" target=main>注册顾客管理</a>
						</dd>
                        <dd>
							<a href="mod.jsp" target=main>修改密码</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>影视资讯管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="newsproclaim_add.jsp" target=main>影视资讯添加</a>
						</dd>
                        <dd>
							<a href="newsproclaim_list.jsp" target=main>影视资讯查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>电影类型管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="dianyingleixing_add.jsp" target=main>电影类型添加</a>
						</dd>
                        <dd>
							<a href="dianyingleixing_list.jsp" target=main>电影类型查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>电影信息管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="dianyingxinxi_add.jsp" target=main>电影信息添加</a>
						</dd>
                        <dd>
							<a href="dianyingxinxi_list.jsp" target=main>电影信息查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>影厅信息管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="yingtingxinxi_add.jsp" target=main>影厅信息添加</a>
						</dd>
                        <dd>
							<a href="yingtingxinxi_list.jsp" target=main>影厅信息查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>排片场次管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="dianyingxinxi_list2.jsp" target=main>排片场次添加</a>
						</dd>
                        <dd>
							<a href="paipianchangci_list.jsp" target=main>排片场次查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>座位信息管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="paipianchangci_list2.jsp" target=main>座位信息添加</a>
						</dd>
                        <dd>
							<a href="zuoweixinxi_list.jsp" target=main>座位信息查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>购票订单管理<i class="icon-angle-right"></i>
						</dt>
                        <dd>
							<a href="goupiaodingdan_list.jsp" target=main>购票订单查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>商品类别管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="shangpinleibie_add.jsp" target=main>商品类别添加</a>
						</dd>
                        <dd>
							<a href="shangpinleibie_list.jsp" target=main>商品类别查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>商品信息管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="shangpinxinxi_add.jsp" target=main>商品信息添加</a>
						</dd>
                        <dd>
							<a href="shangpinxinxi_list.jsp" target=main>商品信息查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>购物订单管理<i class="icon-angle-right"></i>
						</dt>
                        <dd>
							<a href="gouwudingdan_list.jsp" target=main>购物订单查询</a>
						</dd>
					</dl>
				</li>


                <li>
					<dl>
						<dt>
							<i class="icon-font"></i>系统管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
 							<a href="essinfo.jsp" target=main>系统公告管理</a>
 						</dd>
						<dd>
 							<a href="essinfo2.jsp" target=main>关于我们管理</a>
 						</dd>
						<dd>
 							<a href="liuyanban_list.jsp" target=main>留言管理</a>
 						</dd>
					</dl>
				</li>
				
			</ul>
		</nav>
		
		<footer class="side-footer">&copy;版权所有</footer>
		
	</div>
	
</div>
</body>
</html>

