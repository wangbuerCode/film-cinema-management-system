<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<%@page import="util.Info"%>
<%@page import="dao.CommDAO"%>
<%@page import="util.PageManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>登录-后台管理系统</title>

<link rel="icon" href="images/icon/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script src="javascript/jquery.js"></script>
<script src="javascript/public.js"></script>
<script src="javascript/plug-ins/customScrollbar.min.js"></script>
<script src="javascript/pages/login.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
</head>
<script type="text/javascript">
 <%
String error = (String)request.getAttribute("error"); 
if(error!=null)
{
 %>
 alert("用户名或密码错误");
 <%}%>
 
  <%
String random = (String)request.getAttribute("random"); 
if(random!=null)
{
 %>
 alert("验证码错误");
 <%}%>
 
 popheight = 39;

function check()
{
	if(document.form1.username.value=="" || document.form1.password.value=="" || document.form1.pagerandom.value=="")
	{
		alert('请输入完整');
		return false;
	}
}

           function loadimage(){ 
document.getElementById("randImage").src = "image.jsp?"+Math.random(); 
} 

           
           </script>
<body class="login-page">

	<section class="login-contain">
		<header>
			<h1>电影院网上售票系统</h1>
			<p></p>
		</header>
        
		<div class="form-content">
        <form name="form1" method="post" action="javadyywsspxt?ac=adminlogin&a=a">
			<ul>
				<li>
					<div class="form-group">
						<label class="control-label">登录账号：</label>
						<input type="text" placeholder="登录账号..." class="form-control form-underlined"  name="username"/>
					</div>
				</li>
				<li>
					<div class="form-group">
						<label class="control-label">登录密码：</label>
						<input type="password" placeholder="登录密码..." class="form-control form-underlined"  name="password"/>
					</div>
				</li>
                <li>
					<div class="form-group">
						<label class="control-label">权限：</label>
						<select name="qx" id="qx">
              <option value="管理员">管理员</option>
              <option value="工作人员">工作人员</option>
              
            </select>
					</div>
				</li>
                
                <li>
					<div class="form-group">
						<label class="control-label">验证码：</label>
						<input name="pagerandom" type="text" id="pagerandom" style=" height:16px; border:solid 1px #000000; color:#666666;width:40px;" >&nbsp;<a href="javascript:loadimage();"><img alt="看不清请点我！" name="randImage" id="randImage" src="image.jsp" width="54" height="20" border="1" align="absmiddle" style="margin-bottom:0px;"> </a>
					</div>
				</li>
				
				<li>
					<input type="submit"  class="btn btn-lg btn-block" value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;"onClick="return check();">
				</li>
				
			</ul>
            </form>
		</div>
         
	</section>
   
<div class="mask"></div>
<div class="dialog">
	<div class="dialog-hd">
		<strong class="lt-title">标题</strong>
		<a class="rt-operate icon-remove JclosePanel" title="关闭"></a>
	</div>
	<div class="dialog-bd">
		<!--start::-->
		<p>这里是基础弹窗,可以定义文本信息，HTML信息这里是基础弹窗,可以定义文本信息，HTML信息。</p>
		<!--end::-->
	</div>
	<div class="dialog-ft">
		<button class="btn btn-info JyesBtn">确认</button>
		<button class="btn btn-secondary JnoBtn">关闭</button>
	</div>
</div>
</body>
</html>
