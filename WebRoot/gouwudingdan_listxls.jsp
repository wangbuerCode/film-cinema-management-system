<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<%@page import="util.Info"%>
<%@page import="dao.CommDAO"%>
<%@page import="util.PageManager"%>
<%
response.setContentType("application/vnd.ms-excel");
response.addHeader("Content-Disposition", "attachment;filename=gouwudingdan.xls");
%>
<html>
  <head>
    <title>购物订单</title>
  </head>
<body >
  <p>已有购物订单列表：</p>

<table width="100%" border="1" align="center" cellpadding="3" cellspacing="1" bordercolor="#ADCEEF" style="border-collapse:collapse">  
  <tr>
    <td width="30" align="center" bgcolor="#A4B6D7">序号</td>
	
    <td bgcolor='#A4B6D7'>商品编号</td>
    <td bgcolor='#A4B6D7'>商品名称</td>
    <td bgcolor='#A4B6D7'>商品类别</td>
    <td bgcolor='#A4B6D7'>价格</td>
    <td bgcolor='#A4B6D7'>购买数量</td>
    <td bgcolor='#A4B6D7'>总金额</td>
    <td bgcolor='#A4B6D7'>购买人</td>
    <td bgcolor='#A4B6D7'>备注</td>
    
    <td bgcolor='#A4B6D7' width='80' align='center'>是否审核</td>
    <td bgcolor='#A4B6D7' width='80' align='center'>是否支付</td>
    <td width="138" align="center" bgcolor="#A4B6D7">添加时间</td>
  </tr>
  <% 
  	 new CommDAO().delete(request,"gouwudingdan"); 
    String url = "gouwudingdan_list.jsp?1=1"; 
	String sql =  "select * from gouwudingdan where 1=1";
    sql+=" order by id desc";
	ArrayList<HashMap> list = PageManager.getPages(url,15,sql, request); 
	int i=0;
	for(HashMap map:list){ 
	i++;

     %>
  <tr>
    <td width="30" align="center"><%=i %></td>
	
    <td><%=map.get("shangpinbianhao") %></td>
    <td><%=map.get("shangpinmingcheng") %></td>
    <td><%=map.get("shangpinleibie") %></td>
    <td><%=map.get("jiage") %></td>
    <td><%=map.get("goumaishuliang") %></td>
    <td><%=map.get("zongjine") %></td>
    <td><%=map.get("goumairen") %></td>
    <td><%=map.get("beizhu") %></td>
    
    <td align='center'><%=map.get("issh")%></td>
    <td align='center'><%=map.get("iszf")%></td>
    <td width="138" align="center"><%=map.get("addtime") %></td>
  </tr>
  	<%
  }
   %>
</table>
<br>
以上数据共<%=i %>条 
  </body>
</html>


