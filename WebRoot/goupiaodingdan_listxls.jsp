<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<%@page import="util.Info"%>
<%@page import="dao.CommDAO"%>
<%@page import="util.PageManager"%>
<%
response.setContentType("application/vnd.ms-excel");
response.addHeader("Content-Disposition", "attachment;filename=goupiaodingdan.xls");
%>
<html>
  <head>
    <title>购票订单</title>
  </head>
<body >
  <p>已有购票订单列表：</p>

<table width="100%" border="1" align="center" cellpadding="3" cellspacing="1" bordercolor="#ADCEEF" style="border-collapse:collapse">  
  <tr>
    <td width="30" align="center" bgcolor="#A4B6D7">序号</td>
	
    <td bgcolor='#A4B6D7'>座位号</td>
    <td bgcolor='#A4B6D7'>影厅名称</td>
    <td bgcolor='#A4B6D7'>影厅类型</td>
    <td bgcolor='#A4B6D7'>电影编号</td>
    <td bgcolor='#A4B6D7'>电影名称</td>
    <td bgcolor='#A4B6D7'>电影类型</td>
    <td bgcolor='#A4B6D7'>主演</td>
    <td bgcolor='#A4B6D7'>导演</td>
    <td bgcolor='#A4B6D7'>票价</td>
    <td bgcolor='#A4B6D7'>放映开始时间</td>
    <td bgcolor='#A4B6D7'>放映结束时间</td>
    <td bgcolor='#A4B6D7'>开始检票时间</td>
    <td bgcolor='#A4B6D7'>购票人</td>
    <td bgcolor='#A4B6D7'>姓名</td>
    <td bgcolor='#A4B6D7'>手机</td>
    <td bgcolor='#A4B6D7'>验证码</td>
    <td bgcolor='#A4B6D7'>备注</td>
    
    <td bgcolor='#A4B6D7' width='80' align='center'>确认订单</td>
    <td bgcolor='#A4B6D7' width='80' align='center'>是否支付</td>
    <td width="138" align="center" bgcolor="#A4B6D7">添加时间</td>
  </tr>
  <% 
  	 new CommDAO().delete(request,"goupiaodingdan"); 
    String url = "goupiaodingdan_list.jsp?1=1"; 
	String sql =  "select * from goupiaodingdan where 1=1";
    sql+=" order by id desc";
	ArrayList<HashMap> list = PageManager.getPages(url,15,sql, request); 
	int i=0;
	for(HashMap map:list){ 
	i++;

     %>
  <tr>
    <td width="30" align="center"><%=i %></td>
	
    <td><%=map.get("zuoweihao") %></td>
    <td><%=map.get("yingtingmingcheng") %></td>
    <td><%=map.get("yingtingleixing") %></td>
    <td><%=map.get("dianyingbianhao") %></td>
    <td><%=map.get("dianyingmingcheng") %></td>
    <td><%=map.get("dianyingleixing") %></td>
    <td><%=map.get("zhuyan") %></td>
    <td><%=map.get("daoyan") %></td>
    <td><%=map.get("piaojia") %></td>
    <td><%=map.get("fangyingkaishishijian") %></td>
    <td><%=map.get("fangyingjieshushijian") %></td>
    <td><%=map.get("kaishijianpiaoshijian") %></td>
    <td><%=map.get("goupiaoren") %></td>
    <td><%=map.get("xingming") %></td>
    <td><%=map.get("shouji") %></td>
    <td><%=map.get("yanzhengma") %></td>
    <td><%=map.get("beizhu") %></td>
    
    <td align='center'><%=map.get("issh")%></td>
    <td align='center'><%=map.get("iszf")%></td>
    <td width="138" align="center"><%=map.get("addtime") %></td>
  </tr>
  	<%
  }
   %>
</table>
<br>
以上数据共<%=i %>条 
  </body>
</html>


