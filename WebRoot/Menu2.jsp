<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<%@page import="util.Info"%>
<%@page import="dao.CommDAO"%>
<%@page import="util.PageManager"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>

<title>电影院网上售票系统</title>

<link rel="icon" href="images/icon/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script src="javascript/jquery.js"></script>
<script src="javascript/plug-ins/customScrollbar.min.js"></script>
<script src="javascript/plug-ins/echarts.min.js"></script>
<script src="javascript/plug-ins/layerUi/layer.js"></script>
<script src="editor/ueditor.config.js"></script>
<script src="editor/ueditor.all.js"></script>
<script src="javascript/plug-ins/pagination.js"></script>
<script src="javascript/public.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
</head>
<body>
<div class="main-wrap">
	<div class="side-nav">
		<div class="side-logo">
			<div class="logo" style="float:left">
				<span class="logo-ico">
					<i class="i-l-1"></i>
					<i class="i-l-2"></i>
					<i class="i-l-3"></i>
				</span>
				<strong>电影院网上售票系统</strong>
			</div>
		</div>
		
		<nav class="side-menu content mCustomScrollbar" data-mcs-theme="minimal-dark">
			
			<ul>
				<li>
					<dl>
						<dt>
							<i class="icon-columns"></i>密码管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="mod.jsp" target=main>密码管理</a>

						</dd>
						
					</dl>
				</li>
				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>购票订单管理<i class="icon-angle-right"></i>
						</dt>
                        <dd>
							<a href="goupiaodingdan_list.jsp" target=main>购票订单查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>购物订单管理<i class="icon-angle-right"></i>
						</dt>
                        <dd>
							<a href="gouwudingdan_list.jsp" target=main>购物订单查询</a>
						</dd>
					</dl>
				</li>

				<li>
					<dl>
						<dt>
							<i class="icon-inbox"></i>留言板管理<i class="icon-angle-right"></i>
						</dt>
                        <dd>
							<a href="liuyanban_list.jsp" target=main>留言板查询</a>
						</dd>
					</dl>
				</li>


				
			</ul>
		</nav>
		
		<footer class="side-footer">&copy;版权所有</footer>
		
	</div>
	
</div>
</body>
</html>

